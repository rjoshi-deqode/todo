//listner on Add task btn 
document.getElementById('task_input_btn')
    .addEventListener("click", () => {

        let task = document.getElementById('task_input').value
        let date = document.getElementById('date_input').value

        if (!task.length || !date.length) {
            showError("task or date cannot be empty")
            return
        }
        let localtasks = get("tasks")
        set("tasks", [{ task, date, done: false, id: get("id"), checked: false }, ...localtasks])
        set("id", get("id") + 1)

        document.getElementById('task_input').value = ""
        document.getElementById('date_input').value = ""
        show()
    })
document.getElementById("date_input").min = formatDate()
//renderer of the tasks table
const show = () => {
    showUndone()
    showDone()
}
const showDone = () => {
    let tasks = get("tasks")
    document.getElementById('show_tasks_done').innerHTML = ''
    let counter = 0
    for (i in tasks) {
        let task = tasks[i]
        if (task.done != true) continue;
        let number = document.createElement("td")
        number.innerHTML = Number(++counter)

        let checkbox = document.createElement("input");
        checkbox.setAttribute("type", "checkbox");
        checkbox.setAttribute("value", false);
        if (task.checked)
            checkbox.setAttribute("checked", true)
        checkbox.onchange = (e) => {
            toggleChecked(task.id)
        }

        let checktd = document.createElement("td")


        checktd.appendChild(checkbox)

        let taskname = document.createElement("td")
        taskname.innerHTML = task.task

        let date = document.createElement("td")
        date.innerHTML = task.date

        let removeBtn = document.createElement('button')
        removeBtn.innerText = "Remove"
        removeBtn.setAttribute('class', 'btn btn-danger btn-sm')
        removeBtn.onclick = () => removeTask(task.id)

        let doneBtn = document.createElement('button')
        doneBtn.innerText = "Done"
        if (task.done) {
            doneBtn.setAttribute('class', 'btn btn-primary btn-sm')
            doneBtn.innerText = "Move to In-Progress"
        }
        else {
            doneBtn.setAttribute('class', 'btn btn-sm')
            doneBtn.innerText = "Done"

        }
        doneBtn.onclick = () => toggleDone(task.id)

        let action = document.createElement('td')
        action.appendChild(removeBtn)
        action.appendChild(doneBtn)


        let tr = document.createElement("tr")
        tr.appendChild(number)
        tr.appendChild(checktd)
        tr.appendChild(taskname)
        tr.appendChild(date)
        tr.appendChild(action)

        document.getElementById('show_tasks_done')
            .appendChild(tr)
    }

}
const showUndone = () => {
    let tasks = get("tasks")
    document.getElementById('show_tasks_undone').innerHTML = ''
    let counter = 0
    for (i in tasks) {
        let task = tasks[i]
        if (task.done == true) continue;

        let number = document.createElement("td")
        number.innerHTML = Number(++counter)

        let checkbox = document.createElement("input");
        checkbox.setAttribute("type", "checkbox");
        checkbox.setAttribute("value", false);
        if (task.checked)
            checkbox.setAttribute("checked", true)
        checkbox.onchange = (e) => {
            toggleChecked(task.id)
        }

        let checktd = document.createElement("td")


        checktd.appendChild(checkbox)

        let taskname = document.createElement("td")
        taskname.innerHTML = task.task

        let date = document.createElement("td")
        date.innerHTML = task.date

        let removeBtn = document.createElement('button')
        removeBtn.innerText = "Remove"
        removeBtn.setAttribute('class', 'btn btn-danger btn-sm')
        removeBtn.onclick = () => removeTask(task.id)

        let doneBtn = document.createElement('button')
        doneBtn.innerText = "Move to Done"
        if (task.done) {
            doneBtn.setAttribute('class', 'btn btn-primary btn-sm')
            doneBtn.innerText = "Move to In-Progress"
        }
        else {
            doneBtn.setAttribute('class', 'btn btn-sm')
            doneBtn.innerText = "Move to Done"

        }
        doneBtn.onclick = () => toggleDone(task.id)

        let action = document.createElement('td')
        action.appendChild(removeBtn)
        action.appendChild(doneBtn)


        let tr = document.createElement("tr")
        tr.appendChild(number)
        tr.appendChild(checktd)
        tr.appendChild(taskname)
        tr.appendChild(date)
        tr.appendChild(action)

        document.getElementById('show_tasks_undone')
            .appendChild(tr)
    }

}

//listner on remove selected btn
document.getElementById('remove_selected').onclick = () => {
    let checked = get('checked')
    for (id of checked) {
        removeTask(id)
    }
    set('checked', [])
}
//----Some extra functionality----

// document.getElementById('done_selected').onclick = () => {
//     let checked = get('checked')
//     for (id of checked) {
//         toggleDone(id, true)
//         toggleChecked(id)
//     }
//     set('checked', [])
// }

// document.getElementById('un_done_selected').onclick = () => {
//     let checked = get('checked')
//     for (id of checked) {
//         toggleChecked(id)
//         toggleDone(id, false)
//     }
//     set('checked', [])
// }

//show on script load
show()