//utils
const get = (id) => JSON.parse(localStorage.getItem(id))
const set = (id, data) => localStorage.setItem(id, JSON.stringify(data))

//instantiate sample data
get("tasks") || set("tasks", [{ "task": "Task2", "date": formatDate(), "done": false, "id": 4, "checked": false }, { "task": "Task3", "date": formatDate(), "done": true, "id": 3, "checked": false }, { "task": "Task1", "date": formatDate(), "done": false, "id": 1, "checked": false }])
get("id") || set("id", 10)
get("checked") || set("checked", [])

//remove task according to ID
function removeTask(id) {
    let tasks = get('tasks')
    tasks.splice(tasks.map((e) => e.id).indexOf(id), 1)
    set("tasks", tasks)
    show()

}

//To toggle between checked according to ID
function toggleChecked(id) {
    let checked = get('checked')
    let tasks = get('tasks')
    checked.includes(id) ? checked.splice(checked.indexOf(id), 1) : checked.push(id)
    tasks.map((e) => e.id == id ? e.checked = !e.checked : "")
    set('checked', checked)
    set('tasks', tasks)
}

//To toggle between done according to ID
function toggleDone(id, status) {
    console.log(id, status)
    let tasks = get('tasks')
    for (let i = 0; i < tasks.length; i++) {
        if (tasks[i].id == id) {
            tasks[i].done = status ? status : status == false ? false : !tasks[i].done
            break;
        }
    }
    set("tasks", tasks)
    show()
}

function formatDate(date) {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}

function showError(err) {
    document.getElementById("error").innerHTML = err
    setTimeout(() => document.getElementById("error").innerHTML = ''
        , 2000)
}